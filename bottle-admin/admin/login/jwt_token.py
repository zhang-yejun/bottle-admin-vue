# from jwt import jwt,PyJWTError
import jwt
from datetime import datetime,timedelta

JWT_TOKEN_EXPIRE_TIME = 3600 * 2  # token有效时间 2小时
JWT_SECRET = 'abc'   # 加解密密钥
JWT_ALGORITHM = 'HS256'  # 加解密算法

def genToken(data):
	expInt = datetime.utcnow() + timedelta(seconds=3)
	payload = {
	'exp': expInt,
	'data': data 
	}
	token = jwt.encode(payload,JWT_SECRET,algorithm= JWT_ALGORITHM)
	# return bytes.decode(token)
	return token

def verfiyToken(tokenStr):
	try:
		tokenBytes =  tokenStr.encode('utf-8')
		# print(tokenBytes)
		payload = jwt.decode(tokenBytes,JWT_SECRET,algorithms= JWT_ALGORITHM)
		# print(payload)
		return payload
	# except PyJWTError as e:
	except Exception as e:
		print("jwt验证失败:" +str(e))
		return None