import time
import json
from bottle import route,post, get, put, delete, request, response
from .user import User,Captcha
from .jwt_token import genToken,verfiyToken
from .gen_captcha import vieCode
from io import BytesIO
# printrequestdata=False
from ..setting import *



# http://127.0.0.1:8080/captcha.jpg?uuid=46cfceb8-fd57-4cd7-8bd7-3115559cda14
@get('/captcha.jpg')
def captcha():
	uuid = request.query.uuid
	img,code=vieCode().GetCodeImage(size=80, length=4)
	code=''.join(code)
	# print(uuid,code)
	# print(img)
	# img.show()
	file = BytesIO()
	img.save(file, format="JPEG")
	data=file.getvalue()
	resp = make_response(data)
	resp.headers["Content-Type"]  = "image/jpg"
	Captcha(uuid,code).save()
	return resp
@post('/sys/login')
def login():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	username=params.get('username')
	if username==None:username=''
	password=params.get('password')
	if password==None:password=''
	uuid=params.get('uuid')
	if uuid==None:uuid=''
	captcha=params.get('captcha')
	if captcha==None:captcha=''
	print('request.json',request.json)
	print('request.params.dict',request.params.dict)
	# print(request.params.dict.keys())
	# username,password='admin','admin'
	print(request.forms.get('username'))
	print('------------------',username,password,uuid,captcha)
	if username:#bottle bug,will return two times,first is null
		user = User.queryUser(username)
		print(user.username,user.password,user.userId)
		# if Captcha(uuid,captcha).verify():
		if True:
			if (user != None) and (user.verifyPassword(password)):
				token = genToken({'username':username,'password':password,'userId':user.userId})
				returnData = {'code': 0, 'msg': 'success', 'token':token,'username':username,'userId':user.userId}
				return json.dumps(returnData)
			else :
				returnData = {'code': 1, 'msg': 'success', 'data': 'username or password is not correct' }
				return json.dumps(returnData)
		else:
			returnData = {'code': 1, 'msg': 'success', 'data': 'captcha is not correct' }
			return json.dumps(returnData) 
@route('/sys/logout', methods=['GET','POST']) 
def logout():
	#前端要清除localStorage或者sessionStorage里面的用户信息和菜单信息
	returnData = {'code': 0, 'msg': 'success', 'data': ' Bye '}
	# returnData = {'code': 0, 'msg': 'success', 'data': ' Bye '+current_user.username}
	return json.dumps(returnData)