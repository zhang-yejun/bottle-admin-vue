﻿from bottle import route,post, get, put, delete, request, response
import pymysql,json,datetime
from .setting import *
# from .login.jwt_token import genToken,verfiyToken

def json_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime("%Y-%m-%d %X")
    elif isinstance(obj, datetime.date):
        return obj.strftime("%Y-%m-%d")
@get('/sys/config/list')
def configlist():
	like = request.query.paramKey
	page = request.query.page
	limit = request.query.limit
	if limit=='':limit=10
	if page=='':page=1
	# print(like,page,limit)
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_config WHERE  param_key LIKE '%"+like+"%'  LIMIT "+str(limit) +" OFFSET " +str((int(page)-1)*int(limit))
	sqlcount = "SELECT count(*) from sys_config  WHERE  param_key LIKE '%"+like+"%'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'id':dataList1[i][0],'paramKey':dataList1[i][1],'paramValue':dataList1[i][2],'status':dataList1[i][3],'remark':dataList1[i][4]})
		# print(dataList)
		cursor.execute(sqlcount)
		count = cursor.fetchall()[0][0]
	except Exception as e:
		return "Error: unable to fetch data from sys_config"+str(e)
	db.close()
	data={
			'msg': 'success',
			'code': 0,
			'page': {
				'totalCount': count,
				'pageSize': 10,
				'totalPage': 1,
				'currPage': 1,
				'list': dataList
				}
			}
	return json.dumps(data,default=json_encoder)
@post('/sys/config/save')
def configsave():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	paramKey=params.get('paramKey')
	if paramKey==None:paramKey=''
	paramValue=params.get('paramValue')
	if paramValue==None:paramValue=''
	status=params.get('status')
	if status==None:status=''
	remark=params.get('remark')
	if remark==None:remark=''

	
	
	if paramKey:#bottle bug,will return two times,first is null
		db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
		cursor = db.cursor()
		sql = 'insert into sys_config (param_key,param_value,status,remark) values('+"'"+str(paramKey)+"',"+"'"+str(paramValue)+"',"+"'"+str(status)+"',"+"'"+str(remark)+"')"
		if printsql:print(sql)
		try:
			cursor.execute(sql)
			db.commit()
		except Exception as e:
			db.rollback()
			return "Error: unable to save data from sys_config"+str(e)
		db.close()
		
		data={
		  'msg': 'success',
		  'code': 0
		}
		return json.dumps(data,default=json_encoder)
@post('/sys/config/update')
def configupdate():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	id=params.get('id')
	if id==None:id=''
	paramKey=params.get('paramKey')
	if paramKey==None:paramKey=''
	paramValue=params.get('paramValue')
	if paramValue==None:paramValue=''
	status=params.get('status')
	if status==None:status=''
	remark=params.get('remark')
	if remark==None:remark=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="UPDATE sys_config SET param_key='"+str(paramKey)+"',param_value='"+str(paramValue)+"',status='"+str(status)+"',remark='"+str(remark)+"'"+ "WHERE id = '"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_config"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@post('/sys/config/delete')
def configdelete():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	id=params.get('id')
	if id==None:id=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="DELETE FROM sys_config WHERE id='"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_config"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@get('/sys/config/info/<id>')
def configinfo(id=None):
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="SELECT * from sys_config where  id='"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList:
			# print(row)
		config={'id':dataList1[0][0],'paramKey':dataList1[0][1],'paramValue':dataList1[0][2],'status':dataList1[0][3],'remark':dataList1[0][4]}
		data={
				'msg': 'success',
				'code': 0,
				'config': config
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_config"+str(e)
	db.close()
@get('/sys/config/select')
def configselect():
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_config"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'id':dataList1[i][0],'paramKey':dataList1[i][1],'paramValue':dataList1[i][2],'status':dataList1[i][3],'remark':dataList1[i][4]})
		# print(dataList)
		data={
				'msg': 'success',
				'code': 0,
				'list': dataList
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_config"+str(e)
	db.close()	