﻿from bottle import route,post, get, put, delete, request, response
import pymysql,json,datetime
from .setting import *
# from .login.jwt_token import genToken,verfiyToken

def json_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime("%Y-%m-%d %X")
    elif isinstance(obj, datetime.date):
        return obj.strftime("%Y-%m-%d")
@get('/sys/menu/list')
def menulist():
	like = request.query.parentId
	page = request.query.page
	limit = request.query.limit
	if limit=='':limit=10
	if page=='':page=1
	# print(like,page,limit)
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_menu WHERE  parent_id LIKE '%"+like+"%'  LIMIT "+str(limit) +" OFFSET " +str((int(page)-1)*int(limit))
	sqlcount = "SELECT count(*) from sys_menu  WHERE  parent_id LIKE '%"+like+"%'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'menuId':dataList1[i][0],'parentId':dataList1[i][1],'name':dataList1[i][2],'url':dataList1[i][3],'perms':dataList1[i][4],'type':dataList1[i][5],'icon':dataList1[i][6],'orderNum':dataList1[i][7]})
		# print(dataList)
		cursor.execute(sqlcount)
		count = cursor.fetchall()[0][0]
	except Exception as e:
		return "Error: unable to fetch data from sys_menu"+str(e)
	db.close()
	# data={
			# 'msg': 'success',
			# 'code': 0,
			# 'page': {
				# 'totalCount': len(dataList),
				# 'pageSize': 10,
				# 'totalPage': 1,
				# 'currPage': 1,
				# 'list': dataList
				# }
			# }
	data=dataList
	return json.dumps(data,default=json_encoder)
@post('/sys/menu/save')
def menusave():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	parentId=params.get('parentId')
	if parentId==None:parentId=''
	name=params.get('name')
	if name==None:name=''
	url=params.get('url')
	if url==None:url=''
	perms=params.get('perms')
	if perms==None:perms=''
	type=params.get('type')
	if type==None:type=''
	icon=params.get('icon')
	if icon==None:icon=''
	orderNum=params.get('orderNum')
	if orderNum==None:orderNum=''


	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = 'insert into sys_menu (parent_id,name,url,perms,type,icon,order_num) values('+"'"+str(parentId)+"',"+"'"+str(name)+"',"+"'"+str(url)+"',"+"'"+str(perms)+"',"+"'"+str(type)+"',"+"'"+str(icon)+"',"+"'"+str(orderNum)+"')"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to save data from sys_menu"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@post('/sys/menu/update')
def menuupdate():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	menuId=params.get('menuId')
	if menuId==None:menuId=''
	parentId=params.get('parentId')
	if parentId==None:parentId=''
	name=params.get('name')
	if name==None:name=''
	url=params.get('url')
	if url==None:url=''
	perms=params.get('perms')
	if perms==None:perms=''
	type=params.get('type')
	if type==None:type=''
	icon=params.get('icon')
	if icon==None:icon=''
	orderNum=params.get('orderNum')
	if orderNum==None:orderNum=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="UPDATE sys_menu SET parent_id='"+str(parentId)+"',name='"+str(name)+"',url='"+str(url)+"',perms='"+str(perms)+"',type='"+str(type)+"',icon='"+str(icon)+"',order_num='"+str(orderNum)+"'"+ "WHERE menu_id = '"+str(menuId)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_menu"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@post('/sys/menu/delete')
def menudelete():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	menuId=params.get('menuId')
	if menuId==None:menuId=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="DELETE FROM sys_menu WHERE menu_id='"+str(menuId)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_menu"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@get('/sys/menu/info/<id>')
def menuinfo(id=None):
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="SELECT * from sys_menu where  menu_id='"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList:
			# print(row)
		menu={'menuId':dataList1[0][0],'parentId':dataList1[0][1],'name':dataList1[0][2],'url':dataList1[0][3],'perms':dataList1[0][4],'type':dataList1[0][5],'icon':dataList1[0][6],'orderNum':dataList1[0][7]}
		data={
				'msg': 'success',
				'code': 0,
				'menu': menu
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_menu"+str(e)
	db.close()
@get('/sys/menu/select')
def menuselect():
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_menu"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		for row in dataList1:
			print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'menuId':dataList1[i][0],'parentId':dataList1[i][1],'name':dataList1[i][2],'url':dataList1[i][3],'perms':dataList1[i][4],'type':dataList1[i][5],'icon':dataList1[i][6],'orderNum':dataList1[i][7]})
		# print(dataList)
		data={
				'msg': 'success',
				'code': 0,
				'list': dataList
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_menu"+str(e)
	db.close()	

	
@get('/sys/menu/nav')
def menunav():
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	# sql = "SELECT * from sys_menu  WHERE  name LIKE '%"+like+"%'  LIMIT "+str(limit) +" OFFSET " +str((int(page)-1)*int(limit))
	sql = "SELECT * from sys_menu "
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		menu_idname_Dict={}
		for i in range(len(dataList1)):
			menu_idname_Dict[dataList1[i][0]]=dataList1[i][2]
		# print(menu_idname_Dict)
		dataList=[]
		for i in range(len(dataList1)):
			if dataList1[i][1] in menu_idname_Dict.keys():
				dataList.append({'menuId':dataList1[i][0],'parentId':dataList1[i][1],'name':dataList1[i][2],'url':dataList1[i][3],'perms':dataList1[i][4],'type':dataList1[i][5],'icon':dataList1[i][6],'orderNum':dataList1[i][7],'parentName':menu_idname_Dict[dataList1[i][1]],'list':None,'open':None})
			else:#root
				dataList.append({'menuId':dataList1[i][0],'parentId':dataList1[i][1],'name':dataList1[i][2],'url':dataList1[i][3],'perms':dataList1[i][4],'type':dataList1[i][5],'icon':dataList1[i][6],'orderNum':dataList1[i][7],'parentName':None,'list':None,'open':None})
				
		# print(dataList)
		menuList=[]
		for i in range(len(dataList)):
			if dataList[i]['parentId']==0:
				dataList[i]['list']=[]
				menuList.append(dataList[i])
		for i in range(len(menuList)):
			# print(menuList[i]['menuId'])
			# print(menuList[i]['parentId'])
			# print(menuList[i]['name'])
			for j in range(len(dataList)):
				if dataList[j]['parentId']==menuList[i]['menuId']:
					# print(dataList[j]['name'])
					menuList[i]['list'].append(dataList[j])
		# print(menuList)
	except Exception as e:
		return "Error: unable to fetch data from sys_menu"+str(e)
	db.close()
	
	permissions=[
		'sys:schedule:info',
		'sys:menu:update',
		'sys:menu:delete',
		'sys:config:info',
		'sys:menu:list',
		'sys:config:save',
		'sys:config:update',
		'sys:schedule:resume',
		'sys:user:delete',
		'sys:config:list',
		'sys:user:update',
		'sys:role:list',
		'sys:menu:info',
		'sys:menu:select',
		'sys:schedule:update',
		'sys:schedule:save',
		'sys:role:select',
		'sys:user:list',
		'sys:menu:save',
		'sys:role:save',
		'sys:schedule:log',
		'sys:role:info',
		'sys:schedule:delete',
		'sys:role:update',
		'sys:schedule:list',
		'sys:user:info',
		'sys:schedule:run',
		'sys:config:delete',
		'sys:role:delete',
		'sys:user:save',
		'sys:schedule:pause',
		'sys:log:list',
		'sys:oss:all'
	  ]
	return json.dumps({'code':0,'menuList':menuList,'permissions':permissions},default=json_encoder)