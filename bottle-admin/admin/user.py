﻿from bottle import route,post, get, put, delete, request, response
import pymysql,json,datetime
from .setting import *
# from .login.jwt_token import genToken,verfiyToken

def json_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime("%Y-%m-%d %X")
    elif isinstance(obj, datetime.date):
        return obj.strftime("%Y-%m-%d")
@get('/sys/user/list')
def userlist():
	like = request.query.username
	page = request.query.page
	limit = request.query.limit
	if limit=='':limit=10
	if page=='':page=1
	# print(like,page,limit)
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_user WHERE  username LIKE '%"+like+"%'  LIMIT "+str(limit) +" OFFSET " +str((int(page)-1)*int(limit))
	sqlcount = "SELECT count(*) from sys_user  WHERE  username LIKE '%"+like+"%'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'userId':dataList1[i][0],'username':dataList1[i][1],'password':dataList1[i][2],'salt':dataList1[i][3],'email':dataList1[i][4],'mobile':dataList1[i][5],'status':dataList1[i][6],'createUserId':dataList1[i][7],'createTime':dataList1[i][8]})
		# print(dataList)
		cursor.execute(sqlcount)
		count = cursor.fetchall()[0][0]
	except Exception as e:
		return "Error: unable to fetch data from sys_user"+str(e)
	db.close()
	data={
			'msg': 'success',
			'code': 0,
			'page': {
				'totalCount': count,
				'pageSize': 10,
				'totalPage': 1,
				'currPage': 1,
				'list': dataList
				}
			}
	return json.dumps(data,default=json_encoder)
@post('/sys/user/save')
def usersave():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	username=params.get('username')
	if username==None:username=''
	password=params.get('password')
	if password==None:password=''
	salt=params.get('salt')
	if salt==None:salt=''
	email=params.get('email')
	if email==None:email=''
	mobile=params.get('mobile')
	if mobile==None:mobile=''
	status=params.get('status')
	if status==None:status=''
	createUserId=params.get('createUserId')
	if createUserId==None:createUserId=''
	createTime=params.get('createTime')
	if createTime==None:createTime=''

	
	
	if username:#bottle bug,will return two times,first is null
		db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
		cursor = db.cursor()
		sql = 'insert into sys_user (username,password,salt,email,mobile,status,create_user_id,create_time) values('+"'"+str(username)+"',"+"'"+str(password)+"',"+"'"+str(salt)+"',"+"'"+str(email)+"',"+"'"+str(mobile)+"',"+"'"+str(status)+"',"+"'"+str(createUserId)+"',"+"'"+str(createTime)+"')"
		if printsql:print(sql)
		try:
			cursor.execute(sql)
			db.commit()
		except Exception as e:
			db.rollback()
			return "Error: unable to save data from sys_user"+str(e)
		db.close()
		
		data={
		  'msg': 'success',
		  'code': 0
		}
		return json.dumps(data,default=json_encoder)
@post('/sys/user/update')
def userupdate():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	userId=params.get('userId')
	if userId==None:userId=''
	username=params.get('username')
	if username==None:username=''
	password=params.get('password')
	if password==None:password=''
	salt=params.get('salt')
	if salt==None:salt=''
	email=params.get('email')
	if email==None:email=''
	mobile=params.get('mobile')
	if mobile==None:mobile=''
	status=params.get('status')
	if status==None:status=''
	createUserId=params.get('createUserId')
	if createUserId==None:createUserId=''
	createTime=params.get('createTime')
	if createTime==None:createTime=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="UPDATE sys_user SET username='"+str(username)+"',password='"+str(password)+"',salt='"+str(salt)+"',email='"+str(email)+"',mobile='"+str(mobile)+"',status='"+str(status)+"',create_user_id='"+str(createUserId)+"',create_time='"+str(createTime)+"'"+ "WHERE user_id = '"+str(userId)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_user"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@post('/sys/user/delete')
def userdelete():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	userId=params.get('userId')
	if userId==None:userId=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="DELETE FROM sys_user WHERE user_id='"+str(userId)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_user"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@get('/sys/user/info/<id>')
def userinfo(id=None):
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="SELECT * from sys_user where  user_id='"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList:
			# print(row)
		user={'userId':dataList1[0][0],'username':dataList1[0][1],'password':dataList1[0][2],'salt':dataList1[0][3],'email':dataList1[0][4],'mobile':dataList1[0][5],'status':dataList1[0][6],'createUserId':dataList1[0][7],'createTime':dataList1[0][8]}
		data={
				'msg': 'success',
				'code': 0,
				'user': user
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_user"+str(e)
	db.close()
@get('/sys/user/select')
def userselect():
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_user"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'userId':dataList1[i][0],'username':dataList1[i][1],'password':dataList1[i][2],'salt':dataList1[i][3],'email':dataList1[i][4],'mobile':dataList1[i][5],'status':dataList1[i][6],'createUserId':dataList1[i][7],'createTime':dataList1[i][8]})
		# print(dataList)
		data={
				'msg': 'success',
				'code': 0,
				'list': dataList
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_user"+str(e)
	db.close()	