﻿from bottle import route, run
from bottle import post, get, put, delete, request, response
import json

 
@get('/cars')
def getcars():
    cars = [ {'name': 'Audi', 'price': 52642},
        {'name': 'Mercedes', 'price': 57127},
        {'name': 'Skoda', 'price': 9000},
        {'name': 'Volvo', 'price': 29000},
        {'name': 'Bentley', 'price': 350000},
        {'name': 'Citroen', 'price': 21000},
        {'name': 'Hummer', 'price': 41400},
        {'name': 'Volkswagen', 'price': 21600} ]
    return dict(data=cars)

# http://localhost:8080/msg?name=zhangsan&age=20
@get('/msg')
def message():
    name = request.query.name
    age = request.query.age
    return "{0} is {1} years old".format(name, age)
	
@get('/hello')
def hello():
    return "Today is a beautiful dayxxaa" 	
@route('/hello/:para', methods=['GET', 'POST'])
def hello(para):
	return 'hello world. ' + para
	
@get('/books/<id:int>')
def a_book(id):
    return str(id)