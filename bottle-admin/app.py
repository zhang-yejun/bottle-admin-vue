from bottle import route, run, request, get, post,template,response,hook
from admin import *
from front import *
from admin.login.jwt_token import genToken,verfiyToken

@hook('before_request')
def crossBefore():
    REQUEST_METHOD = request.environ.get('REQUEST_METHOD')
    HTTP_ACCESS_CONTROL_REQUEST_METHOD = request.environ.get('HTTP_ACCESS_CONTROL_REQUEST_METHOD')
    if REQUEST_METHOD == 'OPTIONS' and HTTP_ACCESS_CONTROL_REQUEST_METHOD:
        request.environ['REQUEST_METHOD'] = HTTP_ACCESS_CONTROL_REQUEST_METHOD
@hook('after_request')
def crossAfter():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = '*'
	
	
# @hook('before_request')
# def beforeRequest():
	# print('before_request22')
# @hook('after_request')
# def afterRequest():
	# print('after_request222')
@hook('before_request')
def authFielter():
	# print('=======================================')
	# print(request.path)
	# if request.path in ['/captcha.jpg','/login']:
	if request.path in ['/captcha.jpg','/login','/sys/menu/nav','/sys/user/info']:
		return None
	token = request.headers.get('Authorization')
	if not verfiyToken(token):
		print("token verify not ok")
		return None
	print(token)
	return token
if __name__ == '__main__':
	run(host='localhost', port=8080, debug=True,reloader=True)