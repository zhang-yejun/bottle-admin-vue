import requests

response = requests.post(
    'http://localhost:9999/login',
    json={
        'client_id': 'user',
        'client_secret': 'password'
    }
).json()

# type = response['type']
token = response['access_token']
print(token)
# print(token)
# token = f"{response['type']} {response['access_token']}"
token = response['type']+" "+response['access_token']
print(token)
# option 1 - Headers
response =requests.get(
    'http://localhost:9999/jwt_info',
    headers={'Authorization': token}
)
# print(dir(response))
print(response.text)