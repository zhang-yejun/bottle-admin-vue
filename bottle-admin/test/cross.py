from bottle import Bottle,hook
# from bottle import route, run, request, get
from bottle import *

app = Bottle()
#可以解决跨域
@hook('before_request')
def crossBefore():
    REQUEST_METHOD = request.environ.get('REQUEST_METHOD')
    HTTP_ACCESS_CONTROL_REQUEST_METHOD = request.environ.get('HTTP_ACCESS_CONTROL_REQUEST_METHOD')
    if REQUEST_METHOD == 'OPTIONS' and HTTP_ACCESS_CONTROL_REQUEST_METHOD:
        request.environ['REQUEST_METHOD'] = HTTP_ACCESS_CONTROL_REQUEST_METHOD
@hook('after_request')
def crossAfter():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = '*'


@hook('before_request')
def beforeRequest():
	print('before_request22')
@hook('after_request')
def afterRequest():
	print('after_request222')
	
def allow_cross_domain(fn):#不起跨域效果
    def _enable_cors(*args, **kwargs):
        #set cross headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Referer, Accept, Origin, User-Agent'     
        if request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)    
    return _enable_cors
	
 
@route('/helloworld/:yourwords', methods=['GET', 'POST'])
# @allow_cross_domain                                              #在此处加上定义的函数
def hello(yourwords):
	return 'hello world. ' + yourwords

	
@route('/message')
def message():
    return "Today is a beautiful day"  
@route('/hello')
def hello2():
    return "hello,Today is a beautiful day" 
# @route('/user')
# def user():
    # return str([user.name for user in User.select()])
	
	
if __name__ == '__main__':
	# run(host='localhost', port=8080, debug=True)
	run(host='localhost', port=8080, debug=True,reloader=True)
# https://www.wenjiangs.com/doc/bottle-plugins-index

# 跨域测试：随便打开一个网站后打开【开发者工具】，里面的【Console】可以直接输入js代码测试：
# var Authorization= "zU6CUSVs88DjC7hkzpeZGpUpphs=";
# var xhr = new XMLHttpRequest();
# xhr.open('GET', 'http://127.0.0.1:8080/hello');
# xhr.setRequestHeader("Authorization",Authorization);
# xhr.send(null);
# xhr.onload = function(e) {
    # var xhr = e.target;
    # console.log(xhr.responseText);
# }