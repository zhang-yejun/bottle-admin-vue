https://blog.csdn.net/qq_51938362/article/details/127914195

四、SpringBoot远程调用
我先给出Java部分的代码结构，在此处，我们只会使用到 java-demo 模块，在下面一节，就会使用到 gateway 模块。


1. 添加 Python 端业务逻辑
在此处，我们简单的套上了一个 Flask 接口。

@app.route("/py", methods=['get'])
def py1():
	print("hello py")
	return "hello, python service"

2. SpringBoot 的远程调用
java-demo 模块的配置如下
server:
  port: 8081
spring:
  application:
    name: javaservice
  cloud:
    nacos:
      server-addr: localhost:8848
	  

业务逻辑如下

@SpringBootApplication
@RestController
@RequestMapping("/java")
public class JavaDemoApplication {

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    public static void main(String[] args) {
        SpringApplication.run(JavaDemoApplication.class, args);
    }

    @GetMapping
    public String javaService(){
        System.out.println("java service start...");
        String url = "http://pythonservice/py";
        // 发送http请求，实现远程调用
        String ps = restTemplate.getForObject(url, String.class);
        System.out.println("Python service 返回内容: 【 " + ps + "】");
        return "Java远程调用Python: " + ps;
    }
}
访问接口
127.0.0.1:1001/java

五、SpringGateway网关转发
回到我最开始的初衷，我们是想提供深度学习模型的接口，因此还是希望前端直接调用Flask的接口而不是先调Java，Java再调Flask。
而微服务中一般会使用SpringGateway来做请求转发，因此，我们就来做SpringGateway转发到我们的Flask接口。

gateway 模块的配置如下
server:
  port: 10010
spring:
  application:
    name: gateway
  cloud:
    nacos:
      server-addr: localhost:8848 
    gateway:
      routes:
        - id: javaservice 
          uri: lb://javaservice 
          predicates: 
            - Path=/java/**
        - id: pythonservice
          uri: lb://pythonservice
          predicates:
            - Path=/py/**
这里，我们将SpringBoot服务与Flask服务均做了请求转发配置
访问接口
127.0.0.1:1001/py
127.0.0.1:1001/java