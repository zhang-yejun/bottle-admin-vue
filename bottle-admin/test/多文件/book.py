from bottle import route, run
from bottle import post, get, put, delete, request, response
import json

books = [
        {'id': 1, 'title': 'book 1'},
        {'id': 2, 'title': 'book 2'},
        {'id': 3, 'title': 'book 3'},
]

@get('/books')
def books_list():
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(books)

@get('/books/<id:int>')
def a_book(id):
    search = filter(lambda book: book['id'] == id, books)
    obj = next(search, None)

    # https://bottlepy.org/docs/dev/tutorial.html#generating-content
    # returnする値が辞書(もしくはそのサブタイプ)場合、Bottoleが自動的にレスポンスヘッダにapplication/jsonを付けてくれる！
    if obj is not None:
        return obj
    else:
        response.status = 404
        return {}

@post('/books')
def create_book():
    # 泥臭い以下のような書き方もできるけど。。。？
    #try:
    #    body = json.load(request.body)
    #except:
    #    raise ValueError
    #books.append(body)

    # HttpリクエストヘッダのcontentTypeが'application/json'であれば、requset.jsonが利用できる。
    # https://bottlepy.org/docs/dev/tutorial.html#json-content
    books.append(request.json)
    return request.json

@put('/books/<id:int>')
def update_book(id):
    search = filter(lambda book: book['id'] == id, books)
    obj = next(search, None)

    if obj is not None:
        index = books.index(obj)
        books[index] = request.json
        return request.json
    else:
        response.status = 404
        return {}

@delete('/books/<id:int>')
def update_book(id):
    search = filter(lambda book: book['id'] == id, books)
    obj = next(search, None)

    if obj is not None:
        index = books.index(obj)
        del books[index]
        return {}
    else:
        response.status = 404
        return {}