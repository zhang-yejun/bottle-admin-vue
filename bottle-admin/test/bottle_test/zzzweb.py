from bottle import route, run, request, get, post,template,response


@route('/message')
def hello():
    return "Today is a beautiful day"  
@route('/cars')
def getcars():
    cars = [ {'name': 'Audi', 'price': 52642},
        {'name': 'Mercedes', 'price': 57127},
        {'name': 'Skoda', 'price': 9000},
        {'name': 'Volvo', 'price': 29000},
        {'name': 'Bentley', 'price': 350000},
        {'name': 'Citroen', 'price': 21000},
        {'name': 'Hummer', 'price': 41400},
        {'name': 'Volkswagen', 'price': 21600} ]

    return dict(data=cars)

# $ curl "localhost:8080/greet?name=Peter&age=34"	
@get('/msg')
def message():

    name = request.query.name
    age = request.query.age

    return "{0} is {1} years old".format(name, age)
#模板三
@route('/tmp3')
def tmp3():
    data = '我来自模板二数据'
    data1 = ['小明','小红','小黑']
    return template("pages/a.html",name = data, data = data1)  
	
	
def check_login(username, password):
	if username=='admin' and password=="admin":return True
	else:return False
@get('/login') # or @route('/login')
def login():
    return '''
        <form action="/login" method="post">
            Username: <input name="username" type="text" />
            Password: <input name="password" type="password" />
            <input value="Login" type="submit" />
        </form>
    '''
@post('/login') # or @route('/login', method='POST')
def do_login():
	username = request.forms.get('username')
	password = request.forms.get('password')
	if check_login(username, password):
		# return "<p>Your login information was correct.</p>"
		response.set_cookie("account", username, secret='some-secret-key')
		return template("<p>Welcome {{name}}! You are now logged in.</p>", name=username)
	else:
		return "<p>Login failed.</p>"	
@route('/restricted')
def restricted_area():
    username = request.get_cookie("account", secret='some-secret-key')
    if username:
        return template("Hello {{name}}. Welcome back.", name=username)
    else:
        return "You are not logged in. Access denied."	
run(host='localhost', port=8080, debug=True)