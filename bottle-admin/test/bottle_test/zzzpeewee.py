from bottle import Bottle
from bottle_peewee import PeeweePlugin
from peewee import Model, CharField

app = Bottle()
db = PeeweePlugin('sqlite:///:memory:')

class User(Model):
    name = CharField()

    class Meta(object):
        database = db.proxy

app.install(db)


User.create_table(fail_silently=True)
User.create(name='test')
User.create(name='zyj')
print([user for user in User.select()])
print([user.name for user in User.select()])


from bottle import route, run, request, get
@route('/message')
def hello():
    return "Today is a beautiful day"  
@route('/user')
def user():
    return str([user.name for user in User.select()])
run(host='localhost', port=8080, debug=True)