﻿from bottle import route,post, get, put, delete, request, response
import pymysql,json,datetime
from .setting import *
# from .login.jwt_token import genToken,verfiyToken

def json_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime("%Y-%m-%d %X")
    elif isinstance(obj, datetime.date):
        return obj.strftime("%Y-%m-%d")
@get('/${moduleName}/${className}/list')
def ${className}list():
	like = request.query.${likeColumnArg}
	page = request.query.page
	limit = request.query.limit
	if limit=='':limit=10
	if page=='':page=1
	# print(like,page,limit)
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from ${tableName} WHERE  ${likeColumn} LIKE '%"+like+"%'  LIMIT "+str(limit) +" OFFSET " +str((int(page)-1)*int(limit))
	sqlcount = "SELECT count(*) from ${tableName}  WHERE  ${likeColumn} LIKE '%"+like+"%'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append(${listRecorder})
		# print(dataList)
		cursor.execute(sqlcount)
		count = cursor.fetchall()[0][0]
	except Exception as e:
		return "Error: unable to fetch data from ${tableName}"+str(e)
	db.close()
	data={
			'msg': 'success',
			'code': 0,
			'page': {
				'totalCount': count,
				'pageSize': 10,
				'totalPage': 1,
				'currPage': 1,
				'list': dataList
				}
			}
	return json.dumps(data,default=json_encoder)
@post('/${moduleName}/${className}/save')
def ${className}save():
	${saveArgs}
	
	
	if ${column1Args}:#bottle bug,will return two times,first is null
		db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
		cursor = db.cursor()
		${saveSql}
		if printsql:print(sql)
		try:
			cursor.execute(sql)
			db.commit()
		except Exception as e:
			db.rollback()
			return "Error: unable to save data from ${tableName}"+str(e)
		db.close()
		
		data={
		  'msg': 'success',
		  'code': 0
		}
		return json.dumps(data,default=json_encoder)
@post('/${moduleName}/${className}/update')
def ${className}update():
	${updateArgs}
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	${updateSql}
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from ${tableName}"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@post('/${moduleName}/${className}/delete')
def ${className}delete():
	${deleteArgs}
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	${deleteSql}
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from ${tableName}"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@get('/${moduleName}/${className}/info/<id>')
def ${className}info(id=None):
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	${infoSql}
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList:
			# print(row)
		${className}=${infoRecorder}
		data={
				'msg': 'success',
				'code': 0,
				'${className}': ${className}
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from ${tableName}"+str(e)
	db.close()
@get('/${moduleName}/${className}/select')
def ${className}select():
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from ${tableName}"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append(${selectRecorder})
		# print(dataList)
		data={
				'msg': 'success',
				'code': 0,
				'list': dataList
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from ${tableName}"+str(e)
	db.close()	