﻿from bottle import route,post, get, put, delete, request, response
import pymysql,json,datetime
from .setting import *
# from .login.jwt_token import genToken,verfiyToken

def json_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime("%Y-%m-%d %X")
    elif isinstance(obj, datetime.date):
        return obj.strftime("%Y-%m-%d")
@get('/sys/role/list')
def rolelist():
	like = request.query.roleName
	page = request.query.page
	limit = request.query.limit
	if limit=='':limit=10
	if page=='':page=1
	# print(like,page,limit)
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_role WHERE  role_name LIKE '%"+like+"%'  LIMIT "+str(limit) +" OFFSET " +str((int(page)-1)*int(limit))
	sqlcount = "SELECT count(*) from sys_role  WHERE  role_name LIKE '%"+like+"%'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'roleId':dataList1[i][0],'roleName':dataList1[i][1],'remark':dataList1[i][2],'createUserId':dataList1[i][3],'createTime':dataList1[i][4]})
		# print(dataList)
		cursor.execute(sqlcount)
		count = cursor.fetchall()[0][0]
	except Exception as e:
		return "Error: unable to fetch data from sys_role"+str(e)
	db.close()
	data={
			'msg': 'success',
			'code': 0,
			'page': {
				'totalCount': count,
				'pageSize': 10,
				'totalPage': 1,
				'currPage': 1,
				'list': dataList
				}
			}
	return json.dumps(data,default=json_encoder)
@post('/sys/role/save')
def rolesave():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	roleName=params.get('roleName')
	if roleName==None:roleName=''
	remark=params.get('remark')
	if remark==None:remark=''
	createUserId=params.get('createUserId')
	if createUserId==None:createUserId=''
	createTime=params.get('createTime')
	if createTime==None:createTime=''

	
	
	if roleName:#bottle bug,will return two times,first is null
		db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
		cursor = db.cursor()
		sql = 'insert into sys_role (role_name,remark,create_user_id,create_time) values('+"'"+str(roleName)+"',"+"'"+str(remark)+"',"+"'"+str(createUserId)+"',"+"'"+str(createTime)+"')"
		if printsql:print(sql)
		try:
			cursor.execute(sql)
			db.commit()
		except Exception as e:
			db.rollback()
			return "Error: unable to save data from sys_role"+str(e)
		db.close()
		
		data={
		  'msg': 'success',
		  'code': 0
		}
		return json.dumps(data,default=json_encoder)
@post('/sys/role/update')
def roleupdate():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	roleId=params.get('roleId')
	if roleId==None:roleId=''
	roleName=params.get('roleName')
	if roleName==None:roleName=''
	remark=params.get('remark')
	if remark==None:remark=''
	createUserId=params.get('createUserId')
	if createUserId==None:createUserId=''
	createTime=params.get('createTime')
	if createTime==None:createTime=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="UPDATE sys_role SET role_name='"+str(roleName)+"',remark='"+str(remark)+"',create_user_id='"+str(createUserId)+"',create_time='"+str(createTime)+"'"+ "WHERE role_id = '"+str(roleId)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_role"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@post('/sys/role/delete')
def roledelete():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	roleId=params.get('roleId')
	if roleId==None:roleId=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="DELETE FROM sys_role WHERE role_id='"+str(roleId)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_role"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@get('/sys/role/info/<id>')
def roleinfo(id=None):
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="SELECT * from sys_role where  role_id='"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList:
			# print(row)
		role={'roleId':dataList1[0][0],'roleName':dataList1[0][1],'remark':dataList1[0][2],'createUserId':dataList1[0][3],'createTime':dataList1[0][4]}
		data={
				'msg': 'success',
				'code': 0,
				'role': role
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_role"+str(e)
	db.close()
@get('/sys/role/select')
def roleselect():
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_role"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'roleId':dataList1[i][0],'roleName':dataList1[i][1],'remark':dataList1[i][2],'createUserId':dataList1[i][3],'createTime':dataList1[i][4]})
		# print(dataList)
		data={
				'msg': 'success',
				'code': 0,
				'list': dataList
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_role"+str(e)
	db.close()	