﻿from bottle import route,post, get, put, delete, request, response
import pymysql,json,datetime
from .setting import *
# from .login.jwt_token import genToken,verfiyToken

def json_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime("%Y-%m-%d %X")
    elif isinstance(obj, datetime.date):
        return obj.strftime("%Y-%m-%d")
@get('/sys/log/list')
def loglist():
	like = request.query.username
	page = request.query.page
	limit = request.query.limit
	if limit=='':limit=10
	if page=='':page=1
	# print(like,page,limit)
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_log WHERE  username LIKE '%"+like+"%'  LIMIT "+str(limit) +" OFFSET " +str((int(page)-1)*int(limit))
	sqlcount = "SELECT count(*) from sys_log  WHERE  username LIKE '%"+like+"%'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'id':dataList1[i][0],'username':dataList1[i][1],'operation':dataList1[i][2],'method':dataList1[i][3],'params':dataList1[i][4],'time':dataList1[i][5],'ip':dataList1[i][6],'createDate':dataList1[i][7]})
		# print(dataList)
		cursor.execute(sqlcount)
		count = cursor.fetchall()[0][0]
	except Exception as e:
		return "Error: unable to fetch data from sys_log"+str(e)
	db.close()
	data={
			'msg': 'success',
			'code': 0,
			'page': {
				'totalCount': count,
				'pageSize': 10,
				'totalPage': 1,
				'currPage': 1,
				'list': dataList
				}
			}
	return json.dumps(data,default=json_encoder)
@post('/sys/log/save')
def logsave():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	username=params.get('username')
	if username==None:username=''
	operation=params.get('operation')
	if operation==None:operation=''
	method=params.get('method')
	if method==None:method=''
	params=params.get('params')
	if params==None:params=''
	time=params.get('time')
	if time==None:time=''
	ip=params.get('ip')
	if ip==None:ip=''
	createDate=params.get('createDate')
	if createDate==None:createDate=''

	
	
	if username:#bottle bug,will return two times,first is null
		db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
		cursor = db.cursor()
		sql = 'insert into sys_log (username,operation,method,params,time,ip,create_date) values('+"'"+str(username)+"',"+"'"+str(operation)+"',"+"'"+str(method)+"',"+"'"+str(params)+"',"+"'"+str(time)+"',"+"'"+str(ip)+"',"+"'"+str(createDate)+"')"
		if printsql:print(sql)
		try:
			cursor.execute(sql)
			db.commit()
		except Exception as e:
			db.rollback()
			return "Error: unable to save data from sys_log"+str(e)
		db.close()
		
		data={
		  'msg': 'success',
		  'code': 0
		}
		return json.dumps(data,default=json_encoder)
@post('/sys/log/update')
def logupdate():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	id=params.get('id')
	if id==None:id=''
	username=params.get('username')
	if username==None:username=''
	operation=params.get('operation')
	if operation==None:operation=''
	method=params.get('method')
	if method==None:method=''
	params=params.get('params')
	if params==None:params=''
	time=params.get('time')
	if time==None:time=''
	ip=params.get('ip')
	if ip==None:ip=''
	createDate=params.get('createDate')
	if createDate==None:createDate=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="UPDATE sys_log SET username='"+str(username)+"',operation='"+str(operation)+"',method='"+str(method)+"',params='"+str(params)+"',time='"+str(time)+"',ip='"+str(ip)+"',create_date='"+str(createDate)+"'"+ "WHERE id = '"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_log"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@post('/sys/log/delete')
def logdelete():
	params={}
	for k in request.params.dict.keys():
		d=json.loads(k)
		params.update(d)
	id=params.get('id')
	if id==None:id=''

	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="DELETE FROM sys_log WHERE id='"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except Exception as e:
		db.rollback()
		return "Error: unable to delete data from sys_log"+str(e)
	db.close()

	data={
	  'msg': 'success',
	  'code': 0
	}
	return json.dumps(data,default=json_encoder)
@get('/sys/log/info/<id>')
def loginfo(id=None):
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql ="SELECT * from sys_log where  id='"+str(id)+"'"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList:
			# print(row)
		log={'id':dataList1[0][0],'username':dataList1[0][1],'operation':dataList1[0][2],'method':dataList1[0][3],'params':dataList1[0][4],'time':dataList1[0][5],'ip':dataList1[0][6],'createDate':dataList1[0][7]}
		data={
				'msg': 'success',
				'code': 0,
				'log': log
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_log"+str(e)
	db.close()
@get('/sys/log/select')
def logselect():
	db = pymysql.connect(host=dbhost,user=dbuser,password=dbpassword,database=dbdatabase)
	cursor = db.cursor()
	sql = "SELECT * from sys_log"
	if printsql:print(sql)
	try:
		cursor.execute(sql)
		dataList1 = cursor.fetchall()
		# for row in dataList1:
			# print(row)
		dataList=[]
		for i in range(len(dataList1)):
			dataList.append({'id':dataList1[i][0],'username':dataList1[i][1],'operation':dataList1[i][2],'method':dataList1[i][3],'params':dataList1[i][4],'time':dataList1[i][5],'ip':dataList1[i][6],'createDate':dataList1[i][7]})
		# print(dataList)
		data={
				'msg': 'success',
				'code': 0,
				'list': dataList
				}
		return json.dumps(data,default=json_encoder)
	except Exception as e:
		return "Error: unable to fetch data from sys_log"+str(e)
	db.close()	