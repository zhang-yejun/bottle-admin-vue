
import qs from 'querystring'
import axios from 'axios';
import { Loading } from 'element-ui';
import { getToken } from '@/utils/auth'
const request=axios.create({
    baseURL:"http://localhost:8080/",
    timeout:5000
})
// request.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
// 请求拦截器
request.interceptors.request.use(config => {
  if (getToken()) {
    config.headers['Authorization'] = 'Bearer ' + getToken()
  }
  return config
})

//  响应拦截器
// axios.interceptors.response.use(response => {
// 	// loadingPage.close()
// 	if (response.data.code === 200) {
// 		return response.data
// 	}
// 	return response.data
// },
// 	error => {
// 		loadingPage.close()
// 		if (error.response.data.code === 500) {
// 			router.push('/login')
// 		}

// 	}
// )

/**
 * http请求
 */
 export class http {

	/**
	 * get请求
	 * @param url
	 * @param params
	 * @returns {Promise<R>}
	 */
	static get(url, params) {
		return new Promise(async (resolve, reject) => {
			try {
				let query = await qs.stringify(params);
				let res = null;
				if (!params) {
					res = await request.get(url);
				} else {
					res = await request.get(url + '?' + query);
				}
				resolve(res);
			} catch (error) {
				let errorMsg = `请求报错路径： ${url} \n 请求错误信息: ${error}`;
				console.log(errorMsg)
				reject(error);
			}
		});
	}

	/**
	 * POST请求 Content-Type: application/json
	 * @param url
	 * @param params：{'username': "admin",'password': "111111"}
	 * @returns {Promise<R>}
	 */
	static post(url, params) {
		return new Promise(async (resolve, reject) => {
			try {
				
				let res = await request({
								url:url,
								method:'post',
								data:params
							})
				resolve(res);
			} catch (error) {
				let errorMsg = `请求报错路径：${url} \n 请求错误信息: ${error}`;
				reject(error);
			}
		});
	}
	// formData请求方式Content-Type: multipart/form-data
	static postform(url, params) {
		return new Promise(async (resolve, reject) => {
			try {
				let data = new FormData()
				for(var key in params){
					data.append(key,params[key])
				}
				console.log(data);
				let res = await request({
					url:url,
					method:'post',
					data:data
				})
				resolve(res);
			} catch (error) {
				let errorMsg = `请求报错路径：${url} \n 请求错误信息: ${error}`;
				reject(error);
			}
		});
	}
	// url para Content-Type: application/x-www-form-urlencoded
	static posturl(url, params) {
		return new Promise(async (resolve, reject) => {
			try {
				for(var i in params){
					if(params[i]==""){
						delete params[i];
					};
				};

				let query = await qs.stringify(params);
				let res = null;
				if (!params) {
					res = await request.post(url);
				} else {
					res = await request.post(url + '?' + query);
				}

				resolve(res);
			} catch (error) {
				let errorMsg = `请求报错路径：${url} \n 请求错误信息: ${error}`;
				reject(error);
			}
		});
	}
	//???not done
	static put(url,params){
        return new Promise(async (resolve, reject) => {
			try {
				let res =  await request.put(url , params);

				resolve(res);
			} catch (error) {
				let errorMsg = `请求报错路径：${url} \n 请求错误信息: ${error}`;
				reject(error);
			}
		});
    }
    static delete(url,params){
        return new Promise(async (resolve, reject) => {
			try {
				let query = await qs.stringify(params);
				let res = null;
				if (!params) {
					res = await request.delete(url);
				} else {
					res = await request.delete(url + '?' + query);
				}
				
				// let res = await request.delete(url, urlsearchparams);
				resolve(res);
			} catch (error) {
				let errorMsg = `请求报错路径：${url} \n 请求错误信息: ${error}`;
				reject(error);
			}
		});
    }
}