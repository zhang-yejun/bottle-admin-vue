/**
 * 邮箱
 * @param {*} s
 */
export function isEmail (s) {
  return /^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s)
}

/**
 * 手机号码
 * @param {*} s
 */
export function isMobile (s) {
  return /^1[0-9]{10}$/.test(s)
}

/**
 * 电话号码
 * @param {*} s
 */
export function isPhone (s) {
  return /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(s)
}

/**
 * URL地址
 * @param {*} s
 */
export function isURL (s) {
  return /^http[s]?:\/\/.*/.test(s)
}
/**
 * isEmpty
// isEmpty("")
// //true
// isEmpty([])
// //true
// isEmpty({})
// //true
// isEmpty(0)
// //true
// isEmpty(undefined)
// //true
// isEmpty(null)
// //true
// isEmpty(1)
// //false
 */
export function isEmpty (val) {
  // null or undefined
  if (val == null) return true;

  if (typeof val === 'boolean') return false;

  if (typeof val === 'number') return !val;

  if (val instanceof Error) return val.message === '';

  switch (Object.prototype.toString.call(val)) {
    // String or Array
    case '[object String]':
    case '[object Array]':
      return !val.length;

    // Map or Set or File
    case '[object File]':
    case '[object Map]':
    case '[object Set]': {
      return !val.size;
    }
    // Plain Object
    case '[object Object]': {
      return !Object.keys(val).length;
    }
  }

  return false;
}

//
function test(){
  var data = { name: '小花', age: '17岁', sex: '男' };
  for (var a in data) {
      if(data[a]=='17岁'){delete json[a];}
      
      console.log('这是属性名' + a); /*属性名*/
      console.log('属性值' + data[a]); /*属性值*/
  };

  var a = ['橡胶', '苹果', '土豆'];
  for (var x in a) {
      console.log('在数组中的位置 - '+x); /*在数组中的位置*/
      console.log('值 - '+a[x]); /*值*/
  };
}


// Accept: application/json, text/plain, */*
// Accept-Encoding: gzip, deflate, br
// Accept-Language: zh-CN,zh;q=0.9
// Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjZlYmVmNDRiLWQ5MmYtNGYxZC05ZTE3LTAwZjhkZmRmMTJlMyJ9.4Q7Bk--TrVhpUwduWXocz0Wd86aHpi2SaLFH7HaxLy7PY54WyqCwG-UR4ap6qT8TdZB3EVUpxlmmzCidktAGYA
// Connection: keep-alive
// Cookie: sidebarStatus=0; Admin-Token=eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjZlYmVmNDRiLWQ5MmYtNGYxZC05ZTE3LTAwZjhkZmRmMTJlMyJ9.4Q7Bk--TrVhpUwduWXocz0Wd86aHpi2SaLFH7HaxLy7PY54WyqCwG-UR4ap6qT8TdZB3EVUpxlmmzCidktAGYA
// Host: localhost
// Referer: http://localhost/login?redirect=%2Findex
// User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4209.400


// Accept: application/json, text/plain, */*
// Accept-Encoding: gzip, deflate, br
// Accept-Language: zh-CN,zh;q=0.9
// Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjZlYmVmNDRiLWQ5MmYtNGYxZC05ZTE3LTAwZjhkZmRmMTJlMyJ9.4Q7Bk--TrVhpUwduWXocz0Wd86aHpi2SaLFH7HaxLy7PY54WyqCwG-UR4ap6qT8TdZB3EVUpxlmmzCidktAGYA
// Connection: keep-alive
// Cookie: Admin-Token=eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjZlYmVmNDRiLWQ5MmYtNGYxZC05ZTE3LTAwZjhkZmRmMTJlMyJ9.4Q7Bk--TrVhpUwduWXocz0Wd86aHpi2SaLFH7HaxLy7PY54WyqCwG-UR4ap6qT8TdZB3EVUpxlmmzCidktAGYA; sidebarStatus=0
// Host: localhost
// Referer: http://localhost/system/user
// User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4209.400