import axios from 'axios'
import { Message, Loading } from 'element-ui'
import { getToken } from '@/utils/auth'
//使用create方法创建axios实例
const service = axios.create({
  timeout: 7000, // 请求超时时间
  baseURL: 'https://localhost:8080/',
})
// 添加请求拦截器
// request.interceptors.request.use(config => {
//   if (getToken()) {
//     config.headers['Authorization'] = 'Bearer ' + getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
//   }
//   return config
// })
// 添加响应拦截器
// request.interceptors.response.use(response => {
//   loadingInstance.close()
//   // console.log(response)
//   return response.data
// }, error => {
//   console.log('TCL: error', error)
//   const msg = error.Message !== undefined ? error.Message : ''
//   Message({
//     message: '网络错误' + msg,
//     type: 'error',
//     duration: 3 * 1000
//   })
//   loadingInstance.close()
//   return Promise.reject(error)
// })
export default service