// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// mockjs模拟数据开关                 // api: https://github.com/nuysoft/Mock
// if (process.env.NODE_ENV !== 'production') {
//   require('@/mock/index.js')
//   require('@/mock/mockvue.js')
// }

import VueCookie from 'vue-cookie'   
Vue.use(VueCookie)

// 保存整站vuex本地储存初始状态
import store from '@/store'                   // api: https://github.com/vuejs/vuex
Vue.prototype.$store = store;

import Axios from "axios"
Vue.prototype.$axios = Axios;

import { isAuth } from '@/utils'
Vue.prototype.isAuth = isAuth     // 权限方法

// 挂载全局
import httpRequest from '@/utils/httpRequest' // api: https://github.com/axios/axios
Vue.prototype.$http = httpRequest // ajax请求方法

// import VeLine from 'v-charts/lib/line.common' //折线图
// import VeBar from 'v-charts/lib/bar.common' // 条形图
// import VeHistogram from 'v-charts/lib/histogram.common' //柱状图
// import VePie from 'v-charts/lib/pie.common' // 饼图
// Vue.component(VeLine.name, VeLine)
// Vue.component(VeBar.name, VeBar)
// Vue.component(VeHistogram.name, VeHistogram)
// Vue.component(VePie.name, VePie)


// Vue.prototype.$router = router;

Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
