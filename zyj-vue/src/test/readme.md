想要按下 F5 进行运行并且调试,那么就要配置好 launch.json 文件. 先点击 Run -> Open Configurations, 输入以下内容



{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
  
    "configurations": [{
      "name": "Launch",
      "type": "node",
      "request": "launch",
      "program": "${workspaceRoot}/${file}",
      },
    ]
  }


调试mocktest.js:
1.在package.json加上
  "type": "module"
2.然后在终端运行node src/mock/mocktest.js
  或者直接在mocktest.js里运行（F5）
