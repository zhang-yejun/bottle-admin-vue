import Mock from 'mockjs'
import { ssrCompileToFunctions } from 'vue-template-compiler'

// 登录
export function login () {
  return {
    // isOpen: false,
    url: '/sys/login',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0,
      'user':{
        "id": "1",
        "username": "admin",
        "password": "21232f297a57a5a743894a0e4a801fc3",
        "nickname": "程序员青戈",
        "avatarUrl": "http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxIiwiZXhwIjoxNjg3MzE0NTc4fQ.ipq7Lt4lWGyAxHQ8AKXnD6ve1qIo_Gnc_em6k_usC_k",
        "role": "ROLE_ADMIN"},
      'menus':[
        {
          'menuId': 21,
          'parentId': 0,
          'parentName': null,
          'name': 'test',
          'url': 'test',
          'perms': null,
          'type': 1,
          'icon': 'admin',
          'orderNum': 1,
          'open': null,
          'list': null
        },
        {
          'menuId': 22,
          'parentId': 0,
          'parentName': null,
          'name': 'upload',
          'url': 'upload',
          'perms': null,
          'type': 1,
          'icon': 'admin',
          'orderNum': 1,
          'open': null,
          'list': null
        },
        {
          'menuId': 1,
          'parentId': 0,
          'parentName': null,
          'name': '系统管理',
          'url': null,
          'perms': null,
          'type': 0,
          'icon': 'system',
          'orderNum': 0,
          'open': null,
          'list': [
            {
              'menuId': 2,
              'parentId': 1,
              'parentName': null,
              'name': '管理员列表',
              'url': 'sys-user',
              'perms': null,
              'type': 1,
              'icon': 'admin',
              'orderNum': 1,
              'open': null,
              'list': null
            },
            {
              'menuId': 3,
              'parentId': 1,
              'parentName': null,
              'name': '角色管理',
              'url': 'sys-role',
              'perms': null,
              'type': 1,
              'icon': 'role',
              'orderNum': 2,
              'open': null,
              'list': null
            },
            {
              'menuId': 4,
              'parentId': 1,
              'parentName': null,
              'name': '菜单管理',
              'url': 'sys-menu',
              'perms': null,
              'type': 1,
              'icon': 'menu',
              'orderNum': 3,
              'open': null,
              'list': null
            },
            {
              'menuId': 5,
              'parentId': 1,
              'parentName': null,
              'name': 'SQL监控',
              'url': 'http://localhost:8080/renren-fast/druid/sql.html',
              'perms': null,
              'type': 1,
              'icon': 'sql',
              'orderNum': 4,
              'open': null,
              'list': null
            },
            {
              'menuId': 6,
              'parentId': 1,
              'parentName': null,
              'name': '定时任务',
              'url': 'job-schedule',
              'perms': null,
              'type': 1,
              'icon': 'job',
              'orderNum': 5,
              'open': null,
              'list': null
            },
            {
              'menuId': 27,
              'parentId': 1,
              'parentName': null,
              'name': '参数管理',
              'url': 'sys-config',
              'perms': 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete',
              'type': 1,
              'icon': 'config',
              'orderNum': 6,
              'open': null,
              'list': null
            },
            {
              'menuId': 30,
              'parentId': 1,
              'parentName': null,
              'name': '文件上传',
              'url': 'oss-oss',
              'perms': 'sys:oss:all',
              'type': 1,
              'icon': 'oss',
              'orderNum': 6,
              'open': null,
              'list': null
            },
            {
              'menuId': 29,
              'parentId': 1,
              'parentName': null,
              'name': '系统日志',
              'url': 'sys-log',
              'perms': 'sys:log:list',
              'type': 1,
              'icon': 'log',
              'orderNum': 7,
              'open': null,
              'list': null
            }
          ]
        }
      ]
      
    }
  }
}

// 退出
export function logout () {
  return {
    // isOpen: false,
    url: '/sys/logout',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}
