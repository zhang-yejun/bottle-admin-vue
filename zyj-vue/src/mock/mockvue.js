import Mock from 'mockjs'
const data = Mock.mock({    //声明用mock随机生成的数据
    id:"@id()"
})
Mock.mock('http://localhost:8081/api/news','get',{
  "list|10":[
    {
      "fname":"@cfirst()",
      "lname":"@clast()",
      "avatar":"@image('100x100','pink','#fff','png','Hello')",
      "info":"@cparagraph()",
      "age":"@integer(0,100)",
      "address":"@county(true)",
      "yzm":"@string('number',6)"
    }
  ]
})
Mock.mock('http://localhost:8080/api/test','get',data)
// Mock.mock('http://localhost:8080/api/testfile','get',getJsonFile("userinfo.json"))