import { http } from '@/utils/http'

// 查询用户信息列表
export function listUser(query) {
  return http.get('/system/user/list',query)
}

// 查询用户信息详细
export function getUser(userId) {
  return http.get('/system/user/'+userId)
}

// 新增用户信息
export function addUser(data) {
  return http.post('/system/user/',data)
}

// 修改用户信息
export function updateUser(data) {
  return http.put('/system/user/',data)
}

// 删除用户信息
export function delUser(userId) {
	
  return http.delete('/system/user/'+userId)
}
