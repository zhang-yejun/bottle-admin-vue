import request from '@/utils/request'
import { http } from '@/utils/http'
// 登录方法
export function login(username, password, code, uuid) {
  const data = {
    username,
    password,
    code,
    uuid
  }
  // return request({
    // url: '/login',
    // headers: {
      // isToken: false
    // },
    // method: 'post',
    // data: data
  // })
  // return http.post('/login',data)
  // return this.$axios.post('http://localhost:8080/login',data)
  // .then(res=>{
  //   console.log(res);
  // }
  //   )
}

// 注册方法
export function register(data) {
  return request({
    url: '/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/getInfo',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

// 获取验证码
export function getCodeImg2() {
  return request({
    url: '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}
export function getCodeImg() {
  return http.get('/captchaImage')
}